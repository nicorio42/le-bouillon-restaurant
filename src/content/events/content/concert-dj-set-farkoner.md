---
title: Concert - DJ Set Farkoner
date: 2024-08-30T20:00
summary: DJ et producteur, Farkoner est un défenseur incontournable de la culture Bass music et soundsystem à Saint-Étienne et en France mais surtout un artiste complet et prometteur, et il compte bien le faire entendre.
image: "@assets/2023_08_04_FATA_MORGANA-Romain-Guede-1921.jpeg"
---
DJ et producteur, Farkoner est un défenseur incontournable de la culture Bass music et soundsystem à Saint-Étienne et en France mais surtout un artiste complet et prometteur, et il compte bien le faire entendre.

Bien qu'il soit encore relativement discret, ses morceaux sont déjà bien appréciés par des piliers de la scène en France et même au Royaume-Uni (Pangaea, Papa Nugs, Kaval, Azamat B, Tim Karbon…).

Réservation conseillée 06.07.38.31.61

Tarif : Prix Libre

Pour en savoir plus sur Farkoner: [https://www.instagram.com/jonas_farkoner/](https://www.instagram.com/jonas_farkoner/?fbclid=IwZXh0bgNhZW0CMTAAAR3YrgtmI-3OJzDhGM5o_VK6TGZeuSZPj-iRv8FBlb1nJbbH6eUpNURlWVo_aem_T10MhBC16454QkIigCiNpQ)

Pour le découvrir : [https://soundcloud.com/farkoner](https://soundcloud.com/farkoner?fbclid=IwZXh0bgNhZW0CMTAAAR2tAV0vxR2ABZ5JH-AhfYQqmB_WyV3C9Ik8Xtd9IyKSaEaYbx8kfeJkqCs_aem_cmTHnK-J0AruyVMou75SGg)
