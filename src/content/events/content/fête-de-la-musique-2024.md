---
title: FÊTE DE LA MUSIQUE 2024 🎶
date: 2024-06-21T19:00
summary: Venez profiter de la terrasse et son esprit guinguette afin de danser, chanter au bord de l'eau et de fêter ensemble la fête de la musique 2024.
image: "@assets/360_F_336703252_zu643pkRDBHrKT5IewYJnHJE1d9ZHNo7.jpg"
---
Pas de groupe de musique cette année mais nous souhaitons quand même vous faire profiter de la terrasse et son esprit guinguette afin que vous puissiez dansez, chantez au bord de l'eau à partir de 19h30
