---
title: "Soirée cinéma de montagne #1"
date: 2024-09-05T20:00
summary: |-
  Soirée ciné au Bouillon des Eaux des Eaux-Vives

  Lhakpa Sherpa - Des sommets de bravoure
image: "@assets/lhakpa_sherpa_des_sommets_de_bravoure.jpg"
---
Soirée ciné au Bouillon des Eaux des Eaux-Vives

En septembre tous les jeudis, venez visionner un film de montagne sur notre écran géant sous le préau.

Pour cette première soirée:

Lhakpa Sherpa - Des sommets de bravoure

Documentaire de Lucy Walker · 1 h 45 min · 31 juillet 2024

Dans ce documentaire captivant, la première femme népalaise à avoir conquis l'Everest se lance dans l'ascension du sommet pour la dixième et dernière fois
