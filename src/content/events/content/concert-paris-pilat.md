---
title: CONCERT - PARIS PILAT 🎶
date: 2024-08-09T20:00
summary: En concert le 9 Août 2024 sur la terrasse du Bouillon - Brasserie des Eaux-Vives, venez profiter du groupe local PARIS PILAT.
image: "@assets/1000038991.jpg"
---
En concert le 9 Août 2024 à 20h00 sur la terrasse du Bouillon - Brasserie des Eaux-Vives, venez profiter du groupe local PARIS PILAT

Propre composition ambiance Rock

Réservation conseillée 06.07.38.31.61

Tarif : Prix Libre
