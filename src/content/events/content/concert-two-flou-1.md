---
title: CONCERT - TWO FLOU 🎶
date: 2024-09-27T20:00
summary: |-
  CONCERT - TWO FLOU
  Venez profiter de Willy et Linda dans un Duo Saxo & Guitare à partir de 20h00 sur la terrasse du Bouillon des Eaux-Vives.
image: "@assets/Essayez Édition magique.png"
---
CONCERT - TWO FLOU

Pour cette 1ère saison estivale du Bouillon des Eaux-Vives, nous avons commencé les concerts avec le DuoTwo Flou donc quoi de mieux que finir Septembre avec Willy et Linda à partir de 20h00 sur la terrasse du Bouillon des Eaux-Vives.

Réservation conseillée 06.07.38.31.61
Tarif : Prix Libre

• Artistes locaux de Salaise sur Sanne • Duo Saxo & Guitare

**[Découvrir TWO FLOU](https://www.facebook.com/profile.php?id=61553977465506&locale=fr_FR)**
