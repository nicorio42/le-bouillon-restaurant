---
title: Concert Magda et Mython 🎶
date: 2024-09-13T20:00
summary: En concert le 13 septembre 2024 à 20h00 sur la terrasse du Bouillon - Brasserie des Eaux-Vives, venez profiter en 1ère partie de Magda et sa magnifique voix en guitare/voix et en 2nd partie de Mython nouveau dans le monde DJ.
image: "@assets/1000044179.jpg"
---
En concert le 13 septembre 2024 à 20h00 sur la terrasse du Bouillon - Brasserie des Eaux-Vives, venez profiter en 1ère partie de Magda et sa magnifique voix en guitare/voix et en 2nd partie de Mython nouveau dans le monde DJ.

Réservation conseillée 06.07.38.31.61
Tarif : Prix Libre

Retrouvez tous nos événements sur la page : https://lebouillondeseauxvives.fr/programmation/
