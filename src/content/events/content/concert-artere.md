---
title: CONCERT - ARTERE  🎶
date: 2024-08-16T20:00
summary: |-
  CONCERT - ARTERE
  Venez profiter du Duo ARTERE  à partir de 20h00 sur la terrasse du Bouillon - Brasserie des Eaux-Vives.
image: "@assets/Photo-Live-1.jpg"
---
Venez profiter du duo à partir de 20h00 sur la terrasse du Bouillon - Brasserie des Eaux-Vives.

Un duo intimiste, simple et acoustique, lancé à l’été 2021, qui met en lumière la jeune chanteuse suisse Maïla Estoppey et le batteur français Victor Jarry.

Une guitare qui appose avec délicatesse l’harmonie de la chanson et une batterie s’ancrant avec force dans des rythmiques groove. De l’anglais, du français. Quelques percussions et deux voix. C’est tout.

Leur musique s’inscrit dans une thématique oscillant entre les sonorités pop et la variété française et internationale. Un mélange de son et de styles.

Réservation conseillée 06.07.38.31.61

Tarif : Prix Libre

Artère Duo *h[ttps://www.youtube.com/@artere6841](https://www.youtube.com/@artere6841)*

[Découvrir Artère](https://www.livetonight.fr/groupe-musique-dj/24357-artere)
