---
title: SPB RIVER FEST 24 🌊
date: 2024-09-28T09:30
summary: Du 28 au 29 Septembre 2024 profitez d'une compétition nationale de Kayak Freestyle sur le bassin de l'Espace Eaux-Vives
image: "@assets/336536256_643474227616088_8592184131418223766_n.jpg"
---
Le SPB c'est une compétition nationale de kayak freestyle, des borders cross, des animations et surtout du fun des sourires et des bonnes soirées à partager tous ensemble.

Détails et informations à venir

Organisation :  [No Pasa Nada](https://www.facebook.com/nopasanadakayaktrip)
