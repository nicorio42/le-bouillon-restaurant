---
title: "Soirée cinéma de montagne #2"
date: 2024-09-12T20:00
summary: |-
  Soirée ciné au Bouillon des Eaux des Eaux-Vives

  14 x 8000 : Aux sommets de l'impossible
image: "@assets/14_x_8000_aux_sommets_de_l_impossible.png"
---
Soirée ciné au Bouillon des Eaux des Eaux-Vives

En septembre tous les jeudis, venez visionner un film de montagne sur notre écran géant sous le préau.

Pour cette deuxième soirée:
14 x 8000 : Aux sommets de l'impossible

Documentaire de Torquil Jones · 1h 41min · 29 novembre 2021

L'alpiniste Nims Purja veut battre un record historique avec son équipe de sherpas : gravir en sept mois plutôt que sept ans les 14 sommets de plus de 8 000 mètres.
