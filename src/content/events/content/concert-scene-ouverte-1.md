---
title: CONCERT - SCENE OUVERTE 🎶
date: 2024-09-07T19:30
summary: En solo, en duo ou avec tout votre groupe, venez performer sur la terrasse du Bouillon - Brasserie des Eaux-Vives le 7 septembre 2024
image: "@assets/1000039018.png"
---
En solo, en duo ou avec tout votre groupe venez performer sur la terrasse du Bouillon - Brasserie des Eaux-Vives le 7 septembre m2024.

**Loli'Pop** 

*Reprises pop-rock et indie-folk*

Julie et Blandine : chant

Laurent : chant et guitare

Romain : basse et contrebasse.

**Cécyl**

*Reprise Pop Rock de Vianney, Tryo, Stéphan Heicher, Renan Luce, Ben mazué... ainsi que 1 ou 2 compositions personnelles.* 

2 musiciens, 2 guitaristes et 1 chanteur.

**Thoree**

*Composition*

Eric au tambour/vocalises et Jessica à la flûte amérindienne.

LA SCENE EST A VOUS !

Inscriptions et contact par mail [cuisine.lebouillon@gmail.com](mailto:cuisine.lebouillon@gmail.com) avant le 30/08/2024
