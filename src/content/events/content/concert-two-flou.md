---
title: CONCERT - TWO FLOU 🎶
date: 2024-07-14T20:00
summary: |-
  CONCERT - TWO FLOU
  Venez profiter de Willy et Linda dans un Duo Saxo & Guitare à partir de 20h00 sur la terrasse du Bouillon des Eaux-Vives.
image: "@assets/Essayez Édition magique.png"
---
CONCERT - TWO FLOU

Venez profiter de Willy et Linda dans un Duo Saxo & Guitare à partir de 20h00 sur la terrasse du Bouillon des Eaux-Vives.

Réservation conseillée 06.07.38.31.61

Tarif : Prix Libre

Artistes locaux de Salaise sur Sanne

**[Découvrir TWO FLOU](https://www.facebook.com/profile.php?id=61553977465506&locale=fr_FR)**
