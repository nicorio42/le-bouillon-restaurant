---
title: "Soirée cinéma de montagne #3"
date: 2024-09-19T20:00
summary: |-
  Soirée ciné au Bouillon des Eaux des Eaux-Vives

  Déjame vivir
image: "@assets/sys_media_104698.jpg"
---
Soirée ciné au Bouillon des Eaux des Eaux-Vives

En septembre tous les jeudis, venez visionner un film de montagne sur notre écran géant sous le préau.

Pour cette troisième soirée:
Déjame vivir

Documentaire de Sébastien Montaz-Rosset · 2014

Déjame Vivir est une aventure épique vécue sur les sommets d'Europe. Le Mont-Blanc, l'Elbrus et le Cervin, Kilian parviendra-t-il à conquérir les trois montagnes qui le fascinent depuis qu'il est enfant ?
