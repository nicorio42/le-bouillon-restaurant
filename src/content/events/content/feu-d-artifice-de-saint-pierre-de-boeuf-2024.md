---
title: Feu d'artifice de Saint-Pierre-de-Boeuf 2024 🎆🎇
date: 2024-07-13T20:00
summary: Venez profiter du feu d'artifice de Saint-Pierre-de-Bœuf depuis la terrasse du Bouillon des Eaux-Vives
image: "@assets/Feu d'artifice Le Bouillon des Eaux Vives SPB 2024.jpg"
---
Exceptionnellement le restaurant restera ouvert jusqu'à 23h00 afin de profiter de la soirée et du feu d'artifice tirer vers 22h30 (environ).
