---
title: EURO FOOT - FRANCE / POLOGNE ⚽
date: 2024-06-25T18:00
summary: Venez visionner et encourager l'équipe de France contre la Pologne. Diffusion du Match à 18h00.
image: "@assets/1444x920_24-equipes-se-disputent-le-titre-lors-de-l-euro-en-allemagne-du-14-juin-au-14-juillet.webp"
---
Venez visionner et encourager l'équipe de France contre la Pologne. Match de phase de groupes D de l'EURO.

Diffusion du Match à 18h00.

Le programme : [ici](https://www.google.com/search?q=calendrier+euro+2024+match+france&sca_esv=2661ef6c07b945d4&sxsrf=ADLYWIIO9BLdLG-AigZtNMnXM_7iaJBKJg%3A1718780811534&ei=i4NyZoukIPackdUPytyN-AU&oq=programmation+euro+2024+match+&gs_lp=Egxnd3Mtd2l6LXNlcnAiHnByb2dyYW1tYXRpb24gZXVybyAyMDI0IG1hdGNoICoCCAEyBhAAGBYYHjIGEAAYFhgeMgYQABgWGB4yBhAAGBYYHjIIEAAYgAQYogQyCBAAGIAEGKIEMggQABiABBiiBDIIEAAYgAQYogQyCBAAGIAEGKIESOoVUPgCWMALcAF4AZABAJgBlwGgAbcGqgEDMS42uAEByAEA-AEBmAIIoAKUB8ICChAAGLADGNYEGEfCAgUQABiABMICBRAhGKABwgIFECEYnwXCAggQABgIGA0YHpgDAIgGAZAGCJIHAzEuN6AH5zI&sclient=gws-wiz-serp#sie=m;/g/11vjmgm5dy;2;/m/01l10v;dt;fp;1;;;)
