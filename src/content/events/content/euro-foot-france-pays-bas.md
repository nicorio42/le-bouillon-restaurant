---
title: EURO FOOT - FRANCE / PAYS-BAS ⚽
date: 2024-06-21T21:00
summary: Venez visionner et encourager l'équipe de France contre le Pays-Bas. Diffusion du Match à 21h00.
image: "@assets/1444x920_24-equipes-se-disputent-le-titre-lors-de-l-euro-en-allemagne-du-14-juin-au-14-juillet.webp"
---
Après la victoire 1-0 contre l'Autriche venez visionner et encourager l'équipe de France contre le Pays-Bas. Match de phase de groupes D de l'EURO.

Diffusion du Match à 21h00.

Le programme : [ici](https://www.google.com/search?sca_esv=2661ef6c07b945d4&cs=0&sxsrf=ADLYWIJscNJj-x2JLL09iWhWVIPDEmPYkQ:1718834380542&q=Pays-Bas+%E2%80%93+France&stick=H4sIAAAAAAAAAOMwe8S4mJFb4OWPe8JSMxknrTl5jXESI5dAcEF-UUmxb2JJcoZ7UX5pgVASF09AYlFJZnJmQWJeSbFQkFAAlyiSSFixf0FBfl5qXomQDBc3koQQrxQ3F6d-rr6BUU5JurGQJBcHXCVcyjDHuKxCSIqLFWyjkKAUPxevfrq-oWGOuXmWWY6JMc8iVuGAxMpiXafEYoVHDZMV3IoS85JTAXoqzK-_AAAA&ved=2ahUKEwjllrKF1eiGAxXQAxAIHXo9CVcQ4fIBKAh6BAgBECA)
