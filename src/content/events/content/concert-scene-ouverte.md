---
title: CONCERT - SCENE OUVERTE 🎶
date: 2024-07-27T19:30
summary: |-
  En solo, en duo ou avec tout votre groupe, venez performer sur la terrasse du Bouillon - Brasserie des Eaux-Vives le 27 Juillet 2024

  LA SCENE EST A VOUS !
  Inscriptions et contact par mail cuisine.lebouillon@gmail.com avant le 21/07/2024
image: "@assets/Post Facebook Fête de la musique concert 21 juin artistes (1).png"
---
En solo, en duo ou avec tout votre groupe venez performer sur la terrasse du Bouillon - Brasserie des Eaux-Vives le 27 Juillet 2024.

[Histoire de La](https://www.facebook.com/profile.php?id=100083445604688&__cft__[0]=AZXjx_OA6Uz_6ENnwArXf0vFbWZbuml-iS9HudDoFSe2MkXfUnobBJaFdIoe7nf0Iyqx3w78bsHmfz982m0RYsqY8OezZSsuUZl9n_apegN1yNrvKMvitsf1K4tgH2lxT75kjqqMB5Kc16hGpiIXCPBsdbv8NQV1dVhEAZ-8kxEvHA&__tn__=-]K-R) - 1er passage

Anouk et Patrick duo guitare voix de reprises et compos

Thoree - 2ème passage

Groupe éphémère : Magda (Pélussin) Guitare/voix folk américain, Eric (Saint-Pierre-de-Boeuf) Tambour/voix et Jessica ( St Colombe) Flûte amérindienne
