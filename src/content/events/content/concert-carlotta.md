---
title: CONCERT - CARLOTTA 🎶
date: 2024-07-19T19:00
summary: |-
  CONCERT - Carlotta
  Venez profiter de Carlotta et son ukulélé à partir de 20h00 sur la terrasse du Bouillon - Brasserie des Eaux-Vives.
image: "@assets/434332_adae3ac6af1c4c9abd3f1d3f03641963~mv2.webp"
---
Venez profiter de Carlotta et son ukulélé à partir de 20h00 sur la terrasse du Bouillon - Brasserie des Eaux-Vives.

*"Sur des airs de ukulélé, la voix chaleureuse de Carlotta réinterprète en toute intimité des chansons d'hier et d'aujourd'hui."*

Réservation conseillée 06.07.38.31.61

Tarif : Prix Libre

Artiste Lyonnaise

Carlotta musique *[https://youtu.be/dc11toxVJ38](https://youtu.be/dc11toxVJ38)*

[Découvrir Carlotta](https://charlyonstage.wixsite.com/carlotta)
