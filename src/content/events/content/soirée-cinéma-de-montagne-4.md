---
title: "Soirée cinéma de montagne #4"
date: 2024-09-26T20:00
summary: |-
  Soirée ciné au Bouillon des Eaux des Eaux-Vives

  La course aux sommets
image: "@assets/la_course_aux_sommets.jpg"
---
Soirée ciné au Bouillon des Eaux des Eaux-Vives

En septembre tous les jeudis, venez visionner un film de montagne sur notre écran géant sous le préau.

Pour cette quatrième et dernière soirée:
La course aux sommets

Documentaire de Nicholas de Taranto et Götz Werner · 1 h 30 min · 4 octobre 2023

Ueli Steck était l'alpiniste le plus rapide de tous les temps, jusqu'à ce que Dani Arnold batte son record de vitesse sur l'Eiger en 2011. Cet exploit a été le point de départ d'un duel entre les deux grimpeurs sur les grandes faces nord des Alpes, qu'ils ont gravies majoritairement en solo, sans aucun équipement de sécurité.
